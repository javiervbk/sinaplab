-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: back
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividadconcertistica`
--

DROP TABLE IF EXISTS `actividadconcertistica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actividadconcertistica` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaylugar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividadconcertistica`
--

LOCK TABLES `actividadconcertistica` WRITE;
/*!40000 ALTER TABLE `actividadconcertistica` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividadconcertistica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asesorias`
--

DROP TABLE IF EXISTS `asesorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asesorias` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombredealumno` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `objetoasesoria` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombretrabajooproyecto` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `concluyo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medalla` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asesorias`
--

LOCK TABLES `asesorias` WRITE;
/*!40000 ALTER TABLE `asesorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `asesorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creacionobra`
--

DROP TABLE IF EXISTS `creacionobra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `creacionobra` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `tipo_creacion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creacionobra`
--

LOCK TABLES `creacionobra` WRITE;
/*!40000 ALTER TABLE `creacionobra` DISABLE KEYS */;
/*!40000 ALTER TABLE `creacionobra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursosordinarios`
--

DROP TABLE IF EXISTS `cursosordinarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cursosordinarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombredelcurso` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursosordinarios`
--

LOCK TABLES `cursosordinarios` WRITE;
/*!40000 ALTER TABLE `cursosordinarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursosordinarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_generales`
--

DROP TABLE IF EXISTS `datos_generales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datos_generales` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primerapellido` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segundoapellido` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numerodetrabajador` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curp` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correoadicional` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_generales`
--

LOCK TABLES `datos_generales` WRITE;
/*!40000 ALTER TABLE `datos_generales` DISABLE KEYS */;
INSERT INTO `datos_generales` VALUES (1,1,'Javier','Villalobos','Badillo','12345677','VIBJ920101HDFLDV05','javier@fam.unam.mx','2022-08-12 07:58:23','2022-08-11 04:26:02');
/*!40000 ALTER TABLE `datos_generales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distincionesexternas`
--

DROP TABLE IF EXISTS `distincionesexternas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distincionesexternas` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distincionesexternas`
--

LOCK TABLES `distincionesexternas` WRITE;
/*!40000 ALTER TABLE `distincionesexternas` DISABLE KEYS */;
/*!40000 ALTER TABLE `distincionesexternas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distincionesunam`
--

DROP TABLE IF EXISTS `distincionesunam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distincionesunam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distincionesunam`
--

LOCK TABLES `distincionesunam` WRITE;
/*!40000 ALTER TABLE `distincionesunam` DISABLE KEYS */;
/*!40000 ALTER TABLE `distincionesunam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estimulosunam`
--

DROP TABLE IF EXISTS `estimulosunam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estimulosunam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `siglas` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estimulosunam`
--

LOCK TABLES `estimulosunam` WRITE;
/*!40000 ALTER TABLE `estimulosunam` DISABLE KEYS */;
/*!40000 ALTER TABLE `estimulosunam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estrategiaseducaciondistancia`
--

DROP TABLE IF EXISTS `estrategiaseducaciondistancia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estrategiaseducaciondistancia` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `estrategias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meteriales` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `criterios` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estrategiaseducaciondistancia`
--

LOCK TABLES `estrategiaseducaciondistancia` WRITE;
/*!40000 ALTER TABLE `estrategiaseducaciondistancia` DISABLE KEYS */;
/*!40000 ALTER TABLE `estrategiaseducaciondistancia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examenesextraordinarios`
--

DROP TABLE IF EXISTS `examenesextraordinarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `examenesextraordinarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examenesextraordinarios`
--

LOCK TABLES `examenesextraordinarios` WRITE;
/*!40000 ALTER TABLE `examenesextraordinarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `examenesextraordinarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extensionydifusion`
--

DROP TABLE IF EXISTS `extensionydifusion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extensionydifusion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extensionydifusion`
--

LOCK TABLES `extensionydifusion` WRITE;
/*!40000 ALTER TABLE `extensionydifusion` DISABLE KEYS */;
/*!40000 ALTER TABLE `extensionydifusion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacioncomplementaria`
--

DROP TABLE IF EXISTS `informacioncomplementaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `informacioncomplementaria` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `informacion` text COLLATE utf8_unicode_ci,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacioncomplementaria`
--

LOCK TABLES `informacioncomplementaria` WRITE;
/*!40000 ALTER TABLE `informacioncomplementaria` DISABLE KEYS */;
/*!40000 ALTER TABLE `informacioncomplementaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interpretaciondiscos`
--

DROP TABLE IF EXISTS `interpretaciondiscos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interpretaciondiscos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interpretaciondiscos`
--

LOCK TABLES `interpretaciondiscos` WRITE;
/*!40000 ALTER TABLE `interpretaciondiscos` DISABLE KEYS */;
/*!40000 ALTER TABLE `interpretaciondiscos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investigacion`
--

DROP TABLE IF EXISTS `investigacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `investigacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipodeapoyo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechainicioytermino` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investigacion`
--

LOCK TABLES `investigacion` WRITE;
/*!40000 ALTER TABLE `investigacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `investigacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jurado`
--

DROP TABLE IF EXISTS `jurado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jurado` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `tipojurado` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detalle` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jurado`
--

LOCK TABLES `jurado` WRITE;
/*!40000 ALTER TABLE `jurado` DISABLE KEYS */;
/*!40000 ALTER TABLE `jurado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenciasycomisiones`
--

DROP TABLE IF EXISTS `licenciasycomisiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `licenciasycomisiones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombredecomision` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechas` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenciasycomisiones`
--

LOCK TABLES `licenciasycomisiones` WRITE;
/*!40000 ALTER TABLE `licenciasycomisiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `licenciasycomisiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nombramiento`
--

DROP TABLE IF EXISTS `nombramiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nombramiento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombramiento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nombramiento`
--

LOCK TABLES `nombramiento` WRITE;
/*!40000 ALTER TABLE `nombramiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `nombramiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nombramientoacademicadmin`
--

DROP TABLE IF EXISTS `nombramientoacademicadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nombramientoacademicadmin` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nombramiento` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nombramientoacademicadmin`
--

LOCK TABLES `nombramientoacademicadmin` WRITE;
/*!40000 ALTER TABLE `nombramientoacademicadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `nombramientoacademicadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otrastutorias`
--

DROP TABLE IF EXISTS `otrastutorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `otrastutorias` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tematica` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numerodealumnos` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otrastutorias`
--

LOCK TABLES `otrastutorias` WRITE;
/*!40000 ALTER TABLE `otrastutorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `otrastutorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participacionacademias`
--

DROP TABLE IF EXISTS `participacionacademias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participacionacademias` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombreprograma` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcionactividades` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participacionacademias`
--

LOCK TABLES `participacionacademias` WRITE;
/*!40000 ALTER TABLE `participacionacademias` DISABLE KEYS */;
/*!40000 ALTER TABLE `participacionacademias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participacioncomites`
--

DROP TABLE IF EXISTS `participacioncomites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participacioncomites` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechareuniones` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcionactividades` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participacioncomites`
--

LOCK TABLES `participacioncomites` WRITE;
/*!40000 ALTER TABLE `participacioncomites` DISABLE KEYS */;
/*!40000 ALTER TABLE `participacioncomites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participacionintercambios`
--

DROP TABLE IF EXISTS `participacionintercambios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participacionintercambios` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detalledeactividades` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participacionintercambios`
--

LOCK TABLES `participacionintercambios` WRITE;
/*!40000 ALTER TABLE `participacionintercambios` DISABLE KEYS */;
/*!40000 ALTER TABLE `participacionintercambios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participancionalumnoseventos`
--

DROP TABLE IF EXISTS `participancionalumnoseventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participancionalumnoseventos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participancionalumnoseventos`
--

LOCK TABLES `participancionalumnoseventos` WRITE;
/*!40000 ALTER TABLE `participancionalumnoseventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `participancionalumnoseventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pianistaacompañante`
--

DROP TABLE IF EXISTS `pianistaacompañante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pianistaacompañante` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `asignaturaoprofesoresasignado` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repertorioqueabordo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numerodehoras` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombredealumnos` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pianistaacompañante`
--

LOCK TABLES `pianistaacompañante` WRITE;
/*!40000 ALTER TABLE `pianistaacompañante` DISABLE KEYS */;
/*!40000 ALTER TABLE `pianistaacompañante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pride`
--

DROP TABLE IF EXISTS `pride`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pride` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nivel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pride`
--

LOCK TABLES `pride` WRITE;
/*!40000 ALTER TABLE `pride` DISABLE KEYS */;
/*!40000 ALTER TABLE `pride` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remediales`
--

DROP TABLE IF EXISTS `remediales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `remediales` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `nivel` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remediales`
--

LOCK TABLES `remediales` WRITE;
/*!40000 ALTER TABLE `remediales` DISABLE KEYS */;
/*!40000 ALTER TABLE `remediales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen`
--

DROP TABLE IF EXISTS `resumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resumen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `datosgenerales` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombramiento` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sabatico` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pride` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estimulosunam` int DEFAULT NULL,
  `distincionesunam` int DEFAULT NULL,
  `distincionesexternas` int DEFAULT NULL,
  `licencias` int DEFAULT NULL,
  `cursosordinarios` int DEFAULT NULL,
  `participacionalumnos` int DEFAULT NULL,
  `examenesextraordinarios` int DEFAULT NULL,
  `pianistacompañante` int DEFAULT NULL,
  `estrategiasdistancia` int DEFAULT NULL,
  `asesoriaacademica` int DEFAULT NULL,
  `cursosremediales` int DEFAULT NULL,
  `otrastutorias` int DEFAULT NULL,
  `investigacion` int DEFAULT NULL,
  `creacionmusical` int DEFAULT NULL,
  `actividadconcertistica` int DEFAULT NULL,
  `interpretacionincluidasendiscos` int DEFAULT NULL,
  `extensionydifusion` int DEFAULT NULL,
  `superacion` int DEFAULT NULL,
  `participacionjurado` int DEFAULT NULL,
  `participacioncomisiones` int DEFAULT NULL,
  `participacionacademias` int DEFAULT NULL,
  `participacionintercambio` int DEFAULT NULL,
  `nombramientoacademicoadmin` int DEFAULT NULL,
  `informacioncomplementaria` int DEFAULT NULL,
  `resumenhoras` int DEFAULT NULL,
  `proyectoactividades` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen`
--

LOCK TABLES `resumen` WRITE;
/*!40000 ALTER TABLE `resumen` DISABLE KEYS */;
/*!40000 ALTER TABLE `resumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumenhoras`
--

DROP TABLE IF EXISTS `resumenhoras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resumenhoras` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `frentegrupo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `investigacion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `superacion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resumenhorascol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumenhoras`
--

LOCK TABLES `resumenhoras` WRITE;
/*!40000 ALTER TABLE `resumenhoras` DISABLE KEYS */;
/*!40000 ALTER TABLE `resumenhoras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sabatico`
--

DROP TABLE IF EXISTS `sabatico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sabatico` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `informe` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sabatico`
--

LOCK TABLES `sabatico` WRITE;
/*!40000 ALTER TABLE `sabatico` DISABLE KEYS */;
/*!40000 ALTER TABLE `sabatico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siguienteciclo`
--

DROP TABLE IF EXISTS `siguienteciclo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `siguienteciclo` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `actividadesdocencia` text COLLATE utf8_unicode_ci,
  `actividadesinvestigacion` text COLLATE utf8_unicode_ci,
  `actividadesdifusion` text COLLATE utf8_unicode_ci,
  `otrasactividades` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siguienteciclo`
--

LOCK TABLES `siguienteciclo` WRITE;
/*!40000 ALTER TABLE `siguienteciclo` DISABLE KEYS */;
/*!40000 ALTER TABLE `siguienteciclo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superacion`
--

DROP TABLE IF EXISTS `superacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `superacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `tipo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duracion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaylugar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probatorio` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superacion`
--

LOCK TABLES `superacion` WRITE;
/*!40000 ALTER TABLE `superacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `superacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_estimulos`
--

DROP TABLE IF EXISTS `user_estimulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_estimulos` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `estimulo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_estimulos`
--

LOCK TABLES `user_estimulos` WRITE;
/*!40000 ALTER TABLE `user_estimulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_estimulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Javier Villalobos','javiervbk@gmail.com',NULL,'$2y$10$F9nXhNGLt2utxwxuuNAyA.nwjVIqlQzM9G/S9U5UPiTWc9PM3Goiy',NULL,NULL,NULL,NULL,'2022-08-02 10:07:20','2022-08-02 10:07:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-11 22:18:43
