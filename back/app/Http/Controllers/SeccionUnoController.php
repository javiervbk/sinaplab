<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\DatosGenerales;
use Auth;


class SeccionUnoController extends Controller
{
    public function guardarDatosGenerales(Request $request){
        $datosgrales=Auth::user()->datosGenerales;
        if(!is_null($datosgrales)){
            $datosgrales->nombre=$request->nombre;
            $datosgrales->primerapellido=$request->primerapellido;
            $datosgrales->segundoapellido=$request->segundoapellido;
            $datosgrales->numerodetrabajador=$request->numerodetrabajador;
            $datosgrales->curp=$request->curp;
            $datosgrales->correoadicional=$request->correoadicional;
            $datosgrales->save();


        }else{
            $datosgrales=new DatosGenerales();
            $datosgrales->nombre=$request->nombre;
            $datosgrales->primerapellido=$request->primerapellido;
            $datosgrales->segundoapellido=$request->segundoapellido;
            $datosgrales->numerodetrabajador=$request->numerodetrabajador;
            $datosgrales->curp=$request->curp;
            $datosgrales->correoadicional=$request->correoadicional;
            $datosgrales->user_id=Auth::user()->id;
            $datosgrales->save();


        }
        return response()->json([
            "Mensaje"=>"Se ha guardado correctamente la informacion"
        ]);
    }
    public function getDatosGenerales(){
        return response()->json(["datos"=>Auth::user()->datosGenerales]); 
    }
}
