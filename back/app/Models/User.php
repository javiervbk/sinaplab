<?php

namespace App\Models;

 use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;



class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function datosGenerales(){
        return $this->hasOne(DatosGenerales::class,'user_id');
    }
    public function nombramiento(){
        return $this->hasOne(Nombramiento::class,'user_id');
    }
    public function sabatico(){
        return $this->hasOne(Sabatico::class,'user_id');
    }
    public function pride(){
        return $this->hasOne(Pride::class,'user_id');
    }
    
    public function estimulosUnam(){
        return $this->hasMany(EstimulosUNAM::class,'user_id');
    }
    public function distincionesUnam(){
        return $this->hasMany(DistincionesUNAM::class,'user_id');
    }
    public function distincionesExternas(){
        return $this->hasMany(DistincionesExternas::class,'user_id');
    }
    public function licenciasComisiones(){
        return $this->hasMany(Licencias::class,'user_id');
    }
    public function cursosOrdinarios(){
        return $this->hasMany(CursosOrdinarios::class,'user_id');
    }
    public function participacionAlumnos(){
        return $this->hasMany(ParticipancionDeAlumnos::class,'user_id');
    }
    public function examenesExtraordinarios(){
        return $this->hasMany(ExamenesExtraordinarios::class,'user_id');
    }
    public function nombramientoPianista(){
        return $this->hasOne(NombramientoPianista::class,'user_id');
    }
    public function estrategiasEducacionDistancia(){
        return $this->hasOne(EstrategiasEducacionDistancia::class,'user_id');
    }





}
